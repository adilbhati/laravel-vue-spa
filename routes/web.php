<?php


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('invoice', function(){
    return view('invoice');
});

Route::get('{path}','HomeController@index')->where( 'path', '([A-z\d-/_.]+)?' );

Route::get('/admin', 'AdminController@index')->middleware('checkActive');
Route::get('/superadmin', 'SuperAdminController@index')->middleware('checkActive');

//For Email Verification
//Auth::routes(['verify' => true]);
<?php

use Illuminate\Http\Request;

/*php
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResources(['user' => 'API\UserController']);
Route::get('profile', 'API\UserController@profile');
Route::get('findUser', 'API\UserController@search');
Route::put('profile', 'API\UserController@updateProfile');
Route::get('/admin', 'AdminController@index');
Route::get('/superadmin', 'SuperAdminController@index');

Route::group([
    'middleware'=>'auth:api',
    'prefix'=>'question'
], function(){    
    Route::post('askquestion','API\QuestionController@askquestion');
    Route::post('answerquestion/{id}','API\QuestionController@answerquestion');
    Route::get('answer/{id}','API\QuestionController@answer');
    Route::post('upvotequestion','API\QuestionController@upvote');
    Route::get('getquestion','API\QuestionController@getquestion');
    Route::get('userquestion','API\QuestionController@userquestion');
    Route::get('unansweredquestion','API\QuestionController@unansweredquestion');
});

Route::group([
    'middleware'=>['auth:api,checkActive'],
    'prefix'=>'mealplan'
], function(){
    Route::post('addMeal','API\MealPlanController@addMeal');
    Route::get('getMeal/{id}','API\MealPlanController@getMeal');
    Route::get('getMealPlan/{id}','API\MealPlanController@getMealPlanByEnergy');
    Route::get('getMealPlanList','API\MealPlanController@getMealPlanList');
    Route::delete('destroy/{id}','API\MealPlanController@destroy');
    Route::put('update/{id}','API\MealPlanController@update');
});

Route::group([
    'middleware' => ['auth:api,checkActive'],
    'prefix' => 'foodItem'
], function(){
    Route::get('find/{id}','API\FoodItemController@find');
    Route::post('insert','API\FoodItemController@insert');
    //Route::delete('delete/{$id}','API\FoodItemController@destroy');
    Route::get('getAll','API\FoodItemController@getAll');
    Route::get('getByCategory','API\FoodItemController@getByCategory');
    Route::delete('destroy/{id}','API\FoodItemController@destroy');
    Route::put('update/{id}','API\FoodItemController@update');
    Route::get('getItemByCategory/{category}','API\FoodItemController@findItemByCategory');
    //Route::get('findItem', 'API\FoodItemController@search');
});

Route::group([
    'middleware' => 'auth:api',
    'prefix' => 'cpc'
], function(){
    Route::get('getCpcList','API\CpcController@getCpcListByUser');
    Route::get('getTodayCpc','API\CpcController@getTodayCpc');
    Route::get('getcpcindex','API\CpcController@getcpcindex');
    Route::post('addCpc','API\CpcController@addcpc');
});

Route::group([
    'middleware' => ['auth:api'],
    'prefix' => 'rda'
], function(){
    Route::post('addRda','API\UserRdaController@addRda');
    Route::get('getRda/','API\UserRdaController@getRda');
    Route::get('getprofile/','API\UserRdaController@getprofile');
    Route::put('update/{id}','API\UserRdaController@update');
    Route::get('getSingleRda/{id}','API\UserRdaController@getSingleRda');
    Route::put('updateweight','API\UserRdaController@updateWeight');
    Route::delete('destroy/{id}','API\UserRdaController@destroy');
});

Route::group([
    'middleware'=>['auth:api,checkActive'],
    'prefix'=>'recipe'
], function(){
    Route::post('addRecipe','API\RecipeController@addRecipe');
    Route::get('getRecipe/{id}','API\RecipeController@getRecipe');
    Route::get('allRecipe','API\RecipeController@allRecipe');
    Route::put('update/{id}','API\RecipeController@update');
    Route::delete('destroy/{id}','API\RecipeController@destroy');
});

Route::group([
    'middleware'=>['auth:api,checkActive'],
    'prefix'=>'mealplanitem'
], function(){
    Route::post('addmealplanitem','API\MealPlanItemController@addMealPlanItem');
    Route::get('getplanitem/{id}','API\MealPlanItemController@getPlanItem');
    Route::delete('destroy/{id}','API\MealPlanItemController@destroy');
    Route::get('getmealplanindex/','API\MealPlanItemController@getMealPlanIndex');
    Route::get('editplanitem/{id}','API\MealPlanItemController@editplanitem');
    Route::put('updateplanitem/{id}','API\MealPlanItemController@updateplanitem');
});

Route::group([
    'middleware'=>['auth:api,checkActive'],
    'prefix'=>'profile'
], function(){
    Route::get('getprofile','API\UserProfileController@getProfile');
    Route::get('addprofile','API\UserProfileController@addProfile');
    Route::post('updateprofile','API\UserProfileController@updateProfile');    
    
});

Route::group([
    'middleware'=>'auth:api',
    'prefix'=>'dashboard'
], function(){
    Route::get('index','API\DashboardController@index');
    Route::get('adminindex','API\DashboardController@adminindex');
});
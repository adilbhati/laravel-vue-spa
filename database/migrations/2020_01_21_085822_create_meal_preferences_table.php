<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealPreferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meal_preferences', function (Blueprint $table) {
            $table->increments('mealpreference_id');
            $table->string('mealpreference_name');
            //$table->integer('profile_id')->unsigned();
            //$table->foreign('profile_id')->references('profile_id')->on('users_profile')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meal_preferences');
    }
}

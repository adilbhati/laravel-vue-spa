<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCpcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cpc', function (Blueprint $table) {
            $table->increments('cpc_id');
            $table->bigInteger('item_id')->unsigned();
            $table->foreign('item_id')->references('item_id')->on('food_items')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('cpc_item_quantity');
            $table->integer('cpc_item_protein');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->string('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cpc');
    }
}

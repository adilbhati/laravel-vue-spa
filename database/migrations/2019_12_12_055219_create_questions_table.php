<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('question_id');
            $table->string('question_query');
            $table->integer('question_askedby')->unsigned();
            $table->foreign('question_askedby')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('question_answeredby')->unsigned();
            $table->foreign('question_answeredby')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->string('question_answer')->nullable();
            $table->tinyInteger('question_isVisible')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}

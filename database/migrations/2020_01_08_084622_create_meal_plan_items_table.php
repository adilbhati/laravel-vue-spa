<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMealPlanItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meal_plan_items', function (Blueprint $table) {
            $table->bigIncrements('mealplanitem_id');
            $table->string('mealplanitem_name');
            $table->bigInteger('recipe_id')->unsigned();
            $table->foreign('recipe_id')->references('recipe_id')->on('recipes')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('mealplan_id')->unsigned();
            $table->foreign('mealplan_id')->references('mealplan_id')->on('meal_plans')->onDelete('cascade')->onUpdate('cascade');
            $table->bigInteger('mealplanitem_associatedwith')->nullable();
            $table->string('mealplanitem_quantity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meal_plan_items');
    }
}

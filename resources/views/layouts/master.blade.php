<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Nutrela Pro | Eat Healthy. Live Better</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/custom.css">
    <link rel="stylesheet" href="/plugins/summernote/css/summernote.css">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="plugins/sweetalert/css/bootstrap-4.min.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper" id="app">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">

    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>

    </ul>

    <!-- SEARCH FORM -->
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" @keyup="searchit" v-model="search" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" @click="searchit">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
      <img src="./img/logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Nutrela Pro</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="./img/profile.png" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">
              {{Auth::user()->name}}
              <p style="text-transform:capitalize">{{Auth::user()->type}}</p>
          </a>
        </div>
      </div>

      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <router-link to="/dashboard" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt blue"></i>
                <p>
                Dashboard
                </p>
            </router-link>
          </li>
          <!--FoodItem-->
          @if(Gate::check('isAdmin')||Gate::check('isNutrionist'))
          <li class="nav-item has-treeview">  
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-apple-alt green"></i>
                <p>
                Food Item
                <i class="right fa fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                <router-link to="/getFoodItem" class="nav-link">
                  <i class="fas fa-utensils nav-icon"></i>
                  <p>Get Food Items</p>
                </router-link>
                </li>
                <li class="nav-item">
                <router-link to="/addFoodItem" class="nav-link">
                  <i class="fas fa-apple-alt nav-icon"></i>
                  <p>Add Food Items</p>
                </router-link>
                </li>
                <li class="nav-item">
                <router-link to="/getFoodItemByCategory" class="nav-link">
                  <i class="fas fa-utensils nav-icon"></i>
                  <p>Food Items By Category</p>
                </router-link>
                </li>
              </ul>
          </li>          
          <!--Recipe-->
          <li class="nav-item has-treeview">  
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-utensils green"></i>
              <p>
              Recipes
              <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
              <router-link to="/allRecipe" class="nav-link">
                <i class="fas fa-utensils nav-icon"></i>
                <p>All Recipes</p>
              </router-link>
              </li>
              <li class="nav-item">
              <router-link to="/addRecipe" class="nav-link">
                <i class="fas fa-apple-alt nav-icon"></i>
                <p>Add Recipe</p>
              </router-link>
              </li>              
            </ul>
        </li>
          <!--Meal Plan-->
          <li class="nav-item has-treeview">  
            <a href="#" class="nav-link">              
              <i class="nav-icon fas fa-utensils green"></i>
              <p>
              Meal Plan
              <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
              <router-link to="/getMealPlan" class="nav-link">
                <i class="fas fa-apple-alt nav-icon"></i>
                <p>Meal Plan List</p>
              </router-link>
              </li>
              <li class="nav-item">
              <router-link to="/getMealPlanItem" class="nav-link">
                <i class="fas fa-utensils nav-icon"></i>
                <p>Meal Plan Items</p>
              </router-link>
              </li>
              <li class="nav-item">
              <router-link to="/addMealPlan" class="nav-link">                
                <i class="fas fa-apple-alt nav-icon"></i>
                <p>Add Meal Plan</p>
              </router-link>
              </li>
            </ul>
          </li>
          @endif
          <!--Questions-->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-question-circle green"></i>
              <p>
              Questions
              <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                  <router-link to="/question" class="nav-link">
                    <i class="fas fa-question nav-icon"></i>
                    <p>Questions</p>
                  </router-link>
                </li>              
                <li class="nav-item">
                  <router-link to="/askquestion" class="nav-link">
                    <i class="fas fa-question nav-icon"></i>
                    <p>Ask Question</p>
                  </router-link>
                </li>
                <li class="nav-item">
                  <router-link to="/userquestion" class="nav-link">
                    <i class="fas fa-question nav-icon"></i>
                    <p>Your Questions</p>
                  </router-link>
                </li>
                <li class="nav-item">
                  <router-link to="/unansweredquestion" class="nav-link">
                    <i class="fas fa-question nav-icon"></i>
                    <p>Unanswered Questions</p>
                  </router-link>
                </li>
            </ul>
          </li>
          
          <!--Cpc-->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-male green"></i>
              <p>
              CPC
              <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
              <router-link to="/getCpcList" class="nav-link">
              <i class="fas fa-male nav-icon"></i>
              <p>CPC List</p>
              </router-link>
              </li>	  
            </ul>
          </li>
          <!--Rda-->
          <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-info green"></i>
                <p>
                RDA
                <i class="right fa fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <router-link to="/addUserRda" class="nav-link">
                    <i class="fas fa-info nav-icon"></i>
                    <p>Add Rda</p>
                  </router-link>
                </li>
                <li class="nav-item">
                    <router-link to="/GetUserRda" class="nav-link">
                      <i class="fas fa-info nav-icon"></i>
                      <p>Get Rda</p>
                    </router-link>
                  </li>	  
              </ul>
            </li>
          <!--Settings-->
          <li class="nav-item has-treeview">
          @can('isAdmin')
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-cog green"></i>
              <p>
                Settings
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <router-link to="/users" class="nav-link">
                  <i class="fas fa-users nav-icon"></i>
                  <p>Users</p>
                </router-link>
              </li>
              <li class="nav-item">
                <router-link to="/developer" class="nav-link">
                  <i class="nav-icon fas fa-cogs"></i>
                  <p>
                    Developer
                  </p>
                </router-link>
              </li>
          @endcan
              <li class="nav-item">
                <router-link to="/profile" class="nav-link">
                  <i class="nav-icon fas fa-user orange"></i>
                  <p>
                    Profile
                  </p>
                </router-link>
              </li>
            </ul>
          </li>          
          <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
              <i class="nav-icon fa fa-power-off red"></i>
              <p>
                {{ __('Logout') }}
              </p>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
            </form>
          </li>
      </ul>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <router-view></router-view>

        <vue-progress-bar></vue-progress-bar>
        
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2020 <a href="#">Nutrela Pro</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

@auth
<script>
    window.user = @json(auth()->user())
</script>
@endauth

<script src="/js/app.js"></script>
<script src="/js/moment.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<!-- SweetAlert2 -->
<!--<script src="/plugins/sweetalert2/js/sweetalert2.min.js"></script>-->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<!--<script src="/plugins/summernote/js/summernote.min.js"></script>-->
<script src="/js/custom.js"></script>

</body>
</html>

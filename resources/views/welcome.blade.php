<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Nutrela</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
        <link href="{{asset('frontpage/fullpage.min.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css" integrity="sha256-PHcOkPmOshsMBC+vtJdVr5Mwb7r0LkSVJPlPrp/IMpU=" crossorigin="anonymous" />
        <link href="{{asset('css/example.css')}}" rel="stylesheet">
        
        <style>
            .mainmenu {
                list-style-type: none;
                margin: 0;
                padding: 0;
                overflow: hidden;                
                background-color: transparent;
                
                margin-left:50px;
                margin-right:50px;
                margin-top:10px;
                position: fixed;
                top:1px;
                right:1px;
                z-index: 1000000;
                }

                .mainmenu li {
                float: left;
                }

                .mainmenu li a {
                display: block;
                color: #fafafa;
                text-align: center;
                font-weight: 600;
                padding: 14px 16px;
                text-decoration: none;
                }

                .mainmenu li a:hover{
                   
                }

                .custommenu{
                    background:#6c63ff;
                    margin-left:10px;
                    margin-right:10px;
                    padding-left:10px;
                    padding-right:10px;
                    
                    color:#ffffff;
                }

                .custommenu:hover{
                    color:#6c63ff;
                    background:#3f3d56;
                }

        </style>
    </head>
    <body>        
        <!--<ul id="menu">
            <li data-menuanchor="firstPage"><a href="#firstPage">First slide</a></li>
            <li data-menuanchor="secondPage"><a href="#secondPage">Second slide</a></li>
            <li data-menuanchor="3rdPage"><a href="#3rdPage">Third slide</a></li>
            <li data-menuanchor="4thPage"><a href="#4thPage">Forth slide</a></li>
            <li data-menuanchor="lastPage"><a href="#lastPage">Fifth slide</a></li>
        </ul>-->
        
        <ul class="mainmenu">
            <!--<li><a href="index.html">Home</a></li>-->
            @if(Route::has('login'))
                @auth
                <li class="custommenu" style="float:right"><a href="{{ url('/dashboard') }}">Dashboard</a></li>
                @else
                    <li class="custommenu" style="float:right"><a href="{{ url('/home') }}">Login</a></li>
                    <li class="custommenu" style="float:right"><a href="{{ route('register') }}">Register</a></li>
                @endauth
            @endif
          </ul>
        <div id="fullpage">
            <div class="section animated fadeIn" id="section0">
                
                <!--SVG Image-->
                <div style="">
                    <img src="{{asset('img/breakfast.png')}}" width="450"/>
                </div>
                <div class="centerContent">
                    <h1>Welcome to Nutrela Pro</h1>
                    <p>Your guide to healthy diet</p>
                </div>
            </div>
            <div class="section animated fadeIn" id="section1">
                <div class="intro">                    
                    <h1>Create</h1>
                    <p>Your profile and get started</p>
                </div>
            </div>
            <div class="section animated fadeIn" id="section2">
                <div class="intro">
                    <h1>Add Profile</h1>
                    <p>Add your details to get current intake and protein recommendations</p>
                </div>
            </div>
            <div class="section animated fadeIn" id="section3">
                <div class="intro">
                    <h1>Choose a diet plan</h1>
                    <p>Follow our customised diet plans for 1,2 or 3 weeks to get healthier!</p>
                    
                </div>
            </div>
            <div class="section animated fadeIn" id="section4">
                <div class="intro">
                    <h1>Support</h1>
                    <p>Get your doubts cleared in a jiffy with our awesome team of inhouse nutritionists</p>               
                </div>
            </div>
        </div>
        <script src="{{asset('frontpage/fullpage.min.js')}}"></script>
        <script src="{{asset('js/example.js')}}"></script>
        <script type="text/javascript">            
            var myFullpage = new fullpage('#fullpage', {
                sectionsColor: ['#fffafa', '#f5f5f5', '#f8f8ff', '#fffafa', '#ccddff'],
                anchors: ['firstPage', 'secondPage', '3rdPage', '4thpage', 'lastPage'],
                menu: '#menu',
                navigation:true,
                showActiveToolTip:true,

                //equivalent to jQuery `easeOutBack` extracted from http://matthewlein.com/ceaser/
                easingcss3: 'cubic-bezier(0.175, 0.885, 0.320, 1.275)'
            });
        </script>
    </body>
</html>

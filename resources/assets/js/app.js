
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
//import moment from 'moment';

import { Form, HasError, AlertError } from 'vform';

import Gate from "./Gate";
Vue.prototype.$gate = new Gate(window.user);

//Vue.use(require('vue-moment'),{moment});

import swal from 'sweetalert2'
window.swal = swal;

const toast = swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

window.toast = toast;


window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

Vue.component('pagination', require('laravel-vue-pagination'));


import VueRouter from 'vue-router'
Vue.use(VueRouter)

import VueProgressBar from 'vue-progressbar'
Vue.use(VueProgressBar, {
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    height: '3px'
  })

let routes = [
    { path: '/dashboard', component: require('./components/Dashboard.vue'), name:'Dashboard' },
    { path: '/admin', component: require('./components/AdminDashboard.vue') },
    { path: '/superadmin', component: require('./components/SuperAdminDashboard.vue') },
    { path: '/developer', component: require('./components/Developer.vue') },
    { path: '/users', component: require('./components/Users.vue'), name:'Users' },
    { path: '/profile', component: require('./components/Profile.vue') },
    { path: '/getFoodItem', component: require('./components/fooditem/GetFoodItem.vue'), name:'GetFoodItem'  },    
    { path: '/allFoodItem', component: require('./components/fooditem/AllFoodItem.vue')},
    { path: '/addFoodItem', component: require('./components/fooditem/AddFoodItem.vue') ,name:'AddFoodItem' },
    { path: '/editFoodItem/:id', component: require('./components/fooditem/EditFoodItem.vue'), name:'EditFoodItem' },
    { path: '/getFoodItemByCategory', component: require('./components/fooditem/GetFoodItemByCategory.vue'), name:'GetFoodItemByCategory'  },    
    { path: '/getmealplan', component: require('./components/mealplan/GetMealPlan.vue'), name:'GetMealPlan' },
    { path: '/getmealplanitem', component: require('./components/mealplan/GetMealPlanItem.vue'), name:'GetMealPlanItem' },
    { path: '/addmealplanitem', component: require('./components/mealplan/AddMealPlanItem.vue'),name:'AddMealPlanItem' },
    { path: '/editmealplanitem/:id', component: require('./components/mealplan/EditMealPlanItem.vue'), name:'EditMealPlanItem' },
    { path: '/addmealplan', component: require('./components/mealplan/AddMealPlan.vue') , name:'AddMealPlan' }, 
    { path: '/editMealPlan/:id', component: require('./components/mealplan/EditMealPlan.vue'), name:'EditMealPlan' },
    { path: '/getcpclist', component: require('./components/cpc/GetCpcList.vue') , name:'GetCpcList' },
    { path: '/addcpc', component: require('./components/cpc/AddCpc.vue') , name:'AddCpc' },
    { path: '/userrdalist', component: require('./components/rda/UserRdaList.vue') , name:'UserRdaList' },
    { path: '/getUserRda/', component: require('./components/rda/GetUserRda.vue') , name:'GetUserRda' },
    { path: '/editUserRda/:id', component: require('./components/rda/EditUserRda.vue') , name:'EditUserRda' },
    { path: '/addUserRda/', component: require('./components/rda/AddUserRda.vue') , name:'AddUserRda' },
    { path: '/addRecipe/', component: require('./components/recipe/AddRecipe.vue') , name:'AddRecipe' },
    { path: '/allRecipe/', component: require('./components/recipe/AllRecipe.vue') , name:'AllRecipe' },
    { path: '/editRecipe/:id', component: require('./components/recipe/EditRecipe.vue') , name:'EditRecipe' },
    /*{ path: '/editUserRda/:id', component: require('./components/rda/EditUserRda.vue') , name:'EditUserRda' },    */
    { path: '/userprofile/', component: require('./components/profile/UserProfile.vue') , name:'UserProfile' },
    { path: '/addprofile/', component: require('./components/profile/AddProfile.vue') , name:'AddProfile' },
    { path: '/question', component: require('./components/QnA/Question.vue'), name:'Question' },
    { path: '/askquestion', component: require('./components/QnA/AskQuestion.vue'), name:'AskQuestion' },
    { path: '/answerquestion/:id', component: require('./components/QnA/Answer.vue'),name:'Answer'},
    { path: '/userquestion/', component: require('./components/QnA/UserQuestion.vue'),name:'UserQuestion'},
    { path: '/unansweredquestion/', component: require('./components/QnA/UnansweredQuestion.vue'),name:'UnansweredQuestion'},
    
    { path: '*', component: require('./components/NotFound.vue') }    
  ]

const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
  })

Vue.filter('upText', function(text){
    return text.charAt(0).toUpperCase() + text.slice(1)
});

//Vue.filter('myDate',function(created){
//    return moment(created).format('MMMM Do YYYY');
//});

window.Fire =  new Vue();

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue')
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue')
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue')
);

Vue.component(
    'not-found',
    require('./components/NotFound.vue')
);


Vue.component('example-component', require('./components/ExampleComponent.vue'));


const app = new Vue({
    el: '#app',
    router,
    data:{
        search: ''
    },
    methods:{
        searchit: _.debounce(() => {
            Fire.$emit('searching');
        },1000),

        printme() {
            window.print();
        }
    }
});

export default class Gate{

    constructor(user){
        this.user = user;
    }


    isAdmin(){
        return this.user.type === 'admin';
    }

    isUser(){
        return this.user.type === 'user';
    }
    isNutritionist(){
        return this.user.type === 'nutritionist';
    }    
    isAdminOrClient(){
        if(this.user.type === 'admin' || this.user.type === 'client'){
            return true;
        }
    }
    isClientOrUser(){
        if(this.user.type === 'user' || this.user.type === 'client'){
            return true;
        }

    }
    isAdminOrNutritionist(){
        if(this.user.type === 'admin' || this.user.type === 'nutrionist'){
            return true;
        }
    }



}


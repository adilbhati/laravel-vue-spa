<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MealPlanItem extends Model
{
    //create custom primary key
    protected $primaryKey = 'mealplanitem_id';

    //custom created and updated at
    const CREATED_AT = 'mealplanitem_created';
    const UPDATED_AT = 'mealplanitem_updated';

    protected $fillable = ['mealplanitem_name','recipe_id','mealplanitem_associatedwith','mealplan_id','mealplanitem_quantity'];

    //Eloquent Relationships
    //Meal Plan will have multiple recipes
    //Eloquent Format : 'Target Model','This Table','This Id','Target Model Id'
    public function recipe(){
        return $this->belongsToMany('App\Recipe','meal_plan_items', 'mealplanitem_id','recipe_id');
    }

    public function mealplan(){
        return $this->belongsToMany('App\MealPlan','meal_plan_items', 'mealplanitem_id','mealplan_id');
    }

    public function associatedWithParent(){
        return $this->belongsTo('App\MealPlanItem','mealplanitem_associatedWith');
    }

    public function associatedWithChild(){
        return $this->hasOne('App\MealPlanItem','mealplanitem_associatedWith');
    }
}

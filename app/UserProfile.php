<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    //Define Table name if it shows 1146 error
    public $table = "users_profile";

    //created primary key in custom way
    protected $primaryKey = 'profile_id';

    //custom timestamps
    const CREATED_AT = 'profile_created';
    const UPDATED_AT = 'profile_updated';

    protected $fillable = ['user_id','dob','height','weight','gender','mealpreference_id'];

    //Creating Scope
    public function userProfile($query)
    {
        return $query->where('profile_id',Auth()->user()->id);
    }

    //Eloquent Relationships
    //Meal Plan will have multiple recipes
    //Eloquent Format : 'Target Model','This Table','This Id','Target Model Id'
    public function user(){
        return $this->belongsTo('App\User', 'profile_id','id');
    }

    public function mealpreference(){
        return $this->belongsTo('App\MealPreference','profile_id','mealpreference_id');
    }
}

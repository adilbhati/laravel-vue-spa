<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRda extends Model
{
    //
    // primary key is defined in custom way instead of id
    protected $primaryKey = 'rda_id';

    // created_at & updated_at column values defined in custom way
    const CREATED_AT = 'rda_created';
    const UPDATED_AT = 'rda_updated';

    protected $fillable = [
        'user_id','rda_weight','rda_bmi','rda_protein','rda_energy','rda_carbohydrates','rda_fats'
    ];
}

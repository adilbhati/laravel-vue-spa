<?php

namespace App\Http\Middleware;
namespace App\Models;

use Closure;
use App\User;
use Auth;

class CheckActiveUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $active = Auth()->user()->active;  

        if($active = 0){
            return redirect('/dashboard');
        }
        return $next($request);
    }
}

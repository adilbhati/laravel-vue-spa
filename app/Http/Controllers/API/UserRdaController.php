<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use App\UserRda;
use App\User;
use App\UserProfile;
use Auth;
use Illuminate\Http\Request;

class UserRdaController extends Controller
{
    
    //
    
    public function getprofile(){
        $id = Auth()->user()->id;
        $profile = UserProfile::where('user_id',$id)->first();
        return response($profile);
    }

    public function addRda(Request $request){
        $id = Auth()->user()->id;        
        //$id = 1;
        $request->validate([
            'rda_weight'=>'required|string',
            'rda_bmi'=>'required|string',
            'rda_energy'=>'required|string',
            'rda_protein'=>'required|string',
            'rda_carbohydrates'=>'required|string',
            'rda_fats'=>'required|string',
        ]);
        
        $rda = UserRda::create([
            'user_id' => $id,
            'rda_weight' => $request->input('rda_weight'),
            'rda_bmi' => $request->input('rda_bmi'),
            'rda_energy' => $request->input('rda_energy'),
            'rda_fats' => $request->input('rda_fats'),
            'rda_protein' => $request->input('rda_protein'),
            'rda_carbohydrates' => $request->input('rda_carbohydrates'),
        ]);

        return response()->json(['response'=>'success']);
    }

    public function getRda()
    {   
        $id = Auth()->user()->id;     
        $rda = UserRda::where('user_id',$id)->orderBy('rda_created','desc')->get(); 
        return response()->json(collect(['rda'=>$rda]));
    }

    public function update(Request $request, $id)
    {
        $rda = UserRda::findOrFail($id);
        $rda->update($request->all());

        return response()->json($rda) ;
    }

    public function updateWeight(Request $request)
    {
        $id = Auth()->user()->id;
        
        //$id = 1;
        $profile = UserProfile::where('user_id',$id)->first();
        $profile->weight = $request->weight;
        $profile->save();

        return response()->json($profile);
    }

    public function getSingleRda($id)
    {
        $rda = UserRda::findOrFail($id);
        return response()->json($rda);
    }

    public function destroy($id)
    {
        $rda = UserRda::findOrFail($id);
        $rda->delete();
        return '';
    }
}

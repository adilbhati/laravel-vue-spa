<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FoodItem;
use App\User;

class FoodItemController extends Controller
{
    
    public function getAll()
    {
        $items = FoodItem::all();
        return response()->json($items/*collect(['response'=>'success','items'=>$items])*/);
        //return $items->paginate(10);
    }

    //Grouping By Item Category
    public function getByCategory()
    {
        $itemCategories = FoodItem::select('item_category')->distinct()->get();
        // where condition ==> $itemCategories[0]['item_category'];
        // for loop condition ==> count($itemCategories);
        //Query inside forloop ==> $items = FoodItem::where('item_category',$itemCategories[0]['item_category'])->get();
        
        $finalItems = [];
        
        for($i=0; $i<count($itemCategories);$i++)
        {
            $finalItems[$i]['type']=$itemCategories[$i]['item_category'];

            $finalItems[$i]['items']=FoodItem::where('item_category',$itemCategories[$i]['item_category'])->get();
        }
        return $finalItems;
        
        return response()->json($items);
    }

    public function find($id)
    {
        $item = FoodItem::where('item_id', $id)->first();
        //$itemUnits = json_decode(FoodItem::where('item_id', $id)->pluck('item_units'), true);        
        //$itemUnits = json_decode($itemUnits, true);
        //return $itemUnits;
        return response()->json($item);
    }

    public function insert(Request $request)
    {
        $request->validate([
            'item_name' => 'required|string',
            'item_colloquial' => 'required|string',
            'item_pdcaas' => 'required|string',
            'item_protein' => 'required|string',
            'item_deviation' => 'required|string',
            'item_units' => 'required|string',
            'item_category'=>'required|string'
        ]);
                
        $item = FoodItem::create([
            'item_name' => $request->input('item_name'),
            'item_colloquial' => $request->input('item_colloquial'),
            'item_pdcaas' => $request->input('item_pdcaas'),
            'item_protein' => $request->input('item_protein'),
            'item_deviation' => $request->input('item_deviation'),
            'item_units' => $request->input('item_units'),
            'item_category' => $request->input('item_category'),
        ]);
        
        return response()->json(['response'=>'success']);
    }

    public function update(Request $request, $id)
    {
        $fooditem = FoodItem::findOrFail($id);
        $fooditem->update($request->all());

        return response()->json($fooditem) ;
    }

    public function destroy($id)
    {
        $fooditem = FoodItem::findOrFail($id);
        $fooditem->delete();
        return '';
    }

    public function findItemByCategory($category)
    {
        $fooditem = FoodItem::where('item_category',$category)->get();
        return response()->json($fooditem);
    }

    public function search(){

        if ($search = \Request::get('q')) {
            $fooditem = FoodItem::where(function($query) use ($search){
                $query->where('item_name','LIKE',"%$search%")
                        ->orWhere('item_category','LIKE',"%$search%");
            })->paginate(20);
        }else{
            $fooditem = FoodItem::latest()->paginate(20);
        }

        return $fooditem;

    }
    
}

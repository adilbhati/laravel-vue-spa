<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Question;
use App\User;
use Auth;
use App\Http\Controllers\Controller;

class QuestionController extends Controller
{
    public function __construct()
    {     
        //$this->middleware('role:ROLE_SUPERADMIN');
    }
    public function askquestion(Request $request)
    {
        
        $userid = auth()->user()->id;
        //$userid = 1;
        $request->validate([
            'question_query'=>'required|string',            
        ]);
        
        $question=Question::create([
            'question_query' => $request->input('question_query'),
            'question_askedby' => $userid,
        ]);
        
        return response()->json(['response'=>'success']);
    }

    public function answerquestion(Request $request, $id)
    {           
        $userid = auth()->user()->id;
        //$userid = 1;
        $question = Question::findOrFail($id);

        $question->question_answer = $request->input('question_answer');
        $question->question_answeredby = $userid;
        $question->question_isVisible = $request->input('question_isVisible');

        $question->save();

        return response()->json(['response'=>'success']);
    }

    public function upvote(Request $request)
    {
        
    }

    public function getquestion()
    {
        //$id = Auth()->user()->id;
        $question = Question::with('user')->get();
        return response()->json($question);
    }

    public function answer($id){
        $question = Question::where('question_id',$id)->with('user')->first();
        return response()->json($question);
    }

    public function userquestion(){
        $id = Auth()->user()->id;
        $question = Question::where('question_askedby',$id)->get();
        return response()->json($question);
    }

    public function unansweredquestion(){
        
        $question = Question::whereNull('question_answeredby')->get();
        return response()->json($question);
    }
}

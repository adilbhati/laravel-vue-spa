<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MealPlan;
//use App\MealPlanItem;
//use App\Recipe;
use App\User;

class MealPlanController extends Controller
{
    //
    public function getMealPlanList()
    {
        $mealList = MealPlan::all();
        return response()->json($mealList);

    }
    public function addMeal(Request $request){
        
        $request->validate([
            'mealplan_name'=>'required|string',
            'mealplan_description'=>'required|string',
            'mealplan_energy'=>'required|string',            
        ]);
        
        $mealplan = MealPlan::create([            
            'mealplan_name' => $request->input('mealplan_name'),
            'mealplan_description' => $request->input('mealplan_description'),
            'mealplan_energy' => $request->input('mealplan_energy'),            
        ]);
        
        return response()->json(['response'=>'success']);
    }

    public function getMeal($id)
    {
        $mealplan = MealPlan::where('mealplan_id',$id)->first(); 
        return response()->json($mealplan);
            
    }

    public function getMealPlanByEnergy($id)
    {
        $mealplan = MealPlan::where('mealplan_energy',$id)->get();
        
        $mealplanid = MealPlan::where('mealplan_energy',$id)->pluck('mealplan_id');
        
        $mealplanitem = MealPlanItem::where('mealplanitem_id',$mealplanid)->get();
        
        //return $recipeName =  $mealplanitem[0]->recipe;
        
        $recipename = [];
        for($i=0;$i<count($mealplanitem);$i++){
            $recipename[$i] =  $mealplanitem[$i]->recipe->pluck('recipe_name');
        }                
        
        return response()->json(collect(['response'=>'success', 'mealplan'=>$mealplan, 'meal plan item' => $mealplanitem, 'recipe name' => $recipename]));
    }

    public function update(Request $request, $id)
    {
        $mealplan = MealPlan::findOrFail($id);
        $mealplan->update($request->all());

        return $mealplan;
    }

    public function destroy($id)
    {
        $mealplan = MealPlan::findOrFail($id);
        $mealplan->delete();
        return '';
    }

   
}

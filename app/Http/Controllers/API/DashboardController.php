<?php

namespace App\Http\Controllers\API;
use App\FoodItem;
use App\Recipe;
use App\MealPlan;
use App\MealPlanItem;
use App\UserRda;
use App\Cpc;
use App\User;
use App\Question;
use Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    //
    public function index()
    {
        $id = Auth::user()->id;
        //$id = 1;
        
        $rda = UserRda::where('user_id',$id)->latest('rda_protein')->first();
        $recipe = Recipe::count();        
        $mealplan = MealPlan::count();
        //$cpc = Session::get('avgcpc');
        
        return response()->json(collect(['rda'=>$rda,'recipe'=>$recipe,'mealplan'=>$mealplan]));
        
    }

    public function adminindex()
    {
        //This will only work on post request
        //$this->autorize('isAdmin');

        //Users Count
        $users = User::count();
        
        //Food Items Counts
        $fooditem = FoodItem::count();

        //Categories Count
        $foodItemCategories = FoodItem::distinct('item_category')->count('item_category');

        //Questions Count
        $question = Question::count();

        //Unanswered Questions
        $unanswered = Question::where('question_answeredBy',NULL)->count();

        //Meal Plan Items
        $mealplanitem = MealPlanItem::count();

        return response()->json(collect(['users'=>$users,'fooditem'=>$fooditem,'foodItemCategories'=>$foodItemCategories,'question'=>$question,'unansweredquestion'=>$unanswered,'mealplanitem'=>$mealplanitem]));

    }
}

<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MealPlanItem;
use App\User;
use App\Recipe;
use App\MealPlan;


class MealPlanItemController extends Controller
{
    //
    public function getPlanItem($id){
        //$mealplanitem = MealPlanItem::where('mealplan_id',$id)->get();
        $mealplanitem = MealPlanItem::where('mealplan_id',$id)->with('recipe')->with('mealplan')->with('associatedWithChild')->get();
        //$associatedWithName = $mealplanitem->associatedWithChild->mealplanitem_name;
        return response()->json(collect($mealplanitem));
    }

    public function getMealPlanIndex()
    {
        $recipe=Recipe::all();
        $mealplan = MealPlan::all();

        return response()->json(collect(['recipe'=>$recipe,'mealplan'=>$mealplan]));
    }

    public function addMealPlanItem(Request $request)
    {  
        $request->validate([
            'mealplanitem_name'=>'required',
            'recipe_id'=>'required',
            'mealplan_id'=>'required',
            'mealplanitem_associatedwith'=>'required',
            'mealplanitem_quantity'=>'required'
        ]);

        $mealplanitem=MealPlanItem::create([
            'mealplanitem_name' => $request->input('mealplanitem_name'),
            'recipe_id' => $request->input('recipe_id'),
            'mealplan_id' => $request->input('mealplan_id'),
            'mealplanitem_associatedwith' => $request->input('mealplanitem_associatedwith'),
            'mealplanitem_quantity' => $request->input('mealplanitem_quantity'),
        ]);

        return response()->json(['response'=>'success']);
    }

    public function editplanitem($id)
    {
        $mealplanitem = MealPlanItem::where('mealplanitem_id',$id)->with('recipe')->with('mealplan')->with('associatedWithChild')->first();

        return response()->json(collect($mealplanitem));
    }

    public function updateplanitem(Request $request, $id)
    {
        
        $mealplanitem = MealPlanItem::findOrFail($id);                
        $mealplanitem->update($request->all());

        return $mealplanitem;
    }

    public function destroy($id)
    {
        $mealplanitem = MealPlanItem::findOrFail($id);
        $mealplanitem->delete();
        return '';
    }
}

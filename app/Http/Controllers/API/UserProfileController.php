<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserProfile;
use App\User;
use App\MealPreference;

class UserProfileController extends Controller
{   
    
    public function getProfile()
    {
        $id = Auth()->user()->id;        
        $profile = UserProfile::where('user_id',$id)->with('user')->with('mealpreference')->first();
        $mealpreference = MealPreference::all();
        return response()->json(collect(['mealpreference'=>$mealpreference,'profile'=>$profile]));        
    }

    public function addProfile()
    {
        //$id = Auth()->user()->id;        
        //$profile = UserProfile::where('user_id',$id)->with('user')->with('mealpreference')->first();
        
        $mealpreference = MealPreference::all();
        return response()->json(collect(['mealpreference'=>$mealpreference]));        
    }

    public function updateProfile(Request $request)
    {        
        $id = Auth()->user()->id;        
        //$id = 1;
        $profile = UserProfile::where('user_id',$id)->first();        
        if($profile == null){
            $request->validate([
                'dob'=>'required|string',
                'height'=>'required|string',
                'weight'=>'required|string',
                'gender'=>'required|string'
            ]);
            
            $userprofile = UserProfile::create([            
                'dob' => $request->input('dob'),
                'height' => $request->input('height'),
                'weight' => $request->input('weight'),
                'gender' => $request->input('gender'),
                'user_id' => $id,
                'mealpreference_id'=>$request->input('mealpreference_id')
            ]);
            return response()->json($userprofile) ;
        }
        else{

            $profile->update($request->all());
            return response()->json($profile) ;
        }

    }
}

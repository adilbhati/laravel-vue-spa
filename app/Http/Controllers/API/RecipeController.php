<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;
use App\User;
use App\Recipe;
use Auth;
use Illuminate\Http\Request;

class RecipeController extends Controller
{
    //
    public function allRecipe()
    {
        $recipe = Recipe::all();
		return response()->json($recipe);
    }

    public function addRecipe(Request $request){
        
        $request->validate([
            'recipe_name'=>'required|string',
            'recipe_url'=>'required|string',
            'recipe_energy'=>'required|string',
            'recipe_fat'=>'required|string',
            'recipe_protein'=>'required|string',
            'recipe_carbohydrate'=>'required|string',
            'recipe_idealtime'=>'required|string',
        ]);
        
        $recipe = Recipe::create([            
            'recipe_name' => $request->input('recipe_name'),
            'recipe_url' => $request->input('recipe_url'),
            'recipe_energy' => $request->input('recipe_energy'),
            'recipe_fat' => $request->input('recipe_fat'),
            'recipe_protein' => $request->input('recipe_protein'),
            'recipe_carbohydrate' => $request->input('recipe_carbohydrate'),
            'recipe_idealtime' => $request->input('recipe_idealtime'),
        ]);
        
        return response()->json(['response'=>'success']);
    }

    public function getRecipe($id)
    {
        
        $recipe = Recipe::where('recipe_id',$id)->first(); 
        return response()->json($recipe);
    }

    public function update(Request $request, $id)
    {
        $recipe = Recipe::findOrFail($id);
        $recipe->update($request->all());

        return response()->json($recipe) ;
    }

    public function destroy($id)
    {
        $recipe = Recipe::findOrFail($id);
        $recipe->delete();
        return '';
    }
}

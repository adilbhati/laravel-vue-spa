<?php

namespace App\Http\Controllers\API;
use App\FoodItem;
use App\Cpc;
use App\User;
use App\UserProfile;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class CpcController extends Controller
{
    //
    
    public function getCpcListByUser()
    {        
        //if group by doesnt work, change strict to false in config/app.php        

        //create a date varchar field in db, use moment to add date in specific format, and use that column to use groupby
        //groupby using carbon will need exact format in which it is appended
        $id = Auth::user()->id;
        //$id = 1;

        //Get Cpc with food items
        $cpc = Cpc::where('user_id',$id)->with('fooditem')->get();
        
        //Cpc sum with date
        $cpcSum = Cpc::where('user_id',$id)->get()->groupBy('date',true)->map(function($row){
            return [$row->sum('cpc_item_protein')];
        });

        //Get Cpc date->pair it with total cpc perday
        $cpcDate = Cpc::where('user_id',$id)->distinct()->pluck('date');        
        
        //Get Date count
        $cpcCount = Cpc::where('user_id',$id)->distinct('date')->count('date');

        //get cpc date
        $cpcSum1 = Cpc::where('user_id',$id)->distinct('date')->get();

        //get each cpc and add them according to date
        $cpcTotal=0;

        for($i=0; $i < count($cpcSum1); $i++){
            
            $cpcTotal =$cpcTotal + $cpcSum1[$i]['cpc_item_protein'];
        }
        
        //Calculate Average
        $avgcpc = $cpcTotal / $cpcCount;        
        
        return response()->json(collect(['response'=>'success', 'cpc'=>$cpc, 'totalsum' => $cpcSum, 'averageCpc'=>$avgcpc,'cpcDate'=>$cpcDate]));

        //return response()->json(collect(['response'=>'success','cpcSumArray'=>$cpcSumArray,'cpcSum'=>$cpcSum]));
    }

    public function getcpcindex()
    {           
        $id = Auth::user()->id;
        //$id = 1;
        $fooditem=FoodItem::all();
        $profile = UserProfile::where('user_id',$id)->first();
        return response()->json(collect(['fooditem'=>$fooditem,'profile'=>$profile]));
    }

    public function getTodayCpc()
    {
        $id = Auth::user()->id;
        //$id = 1;
        $cpc = Cpc::where('user_id',$id)->whereDate('cpc_created', Carbon::today())->with('fooditem')->get();

        return response()->json(collect($cpc));
    }

    public function addcpc(Request $request)
    {
        $id = Auth::user()->id;
        //$id = 1;
        $request->validate([
            'item_id'=>'required|string',
            'cpc_item_quantity'=>'required|string',
            'cpc_item_protein'=>'required|string',
            'date'=>'required|string'      
        ]);

        $cpc = Cpc::create([            
            'item_id' => $request->input('item_id'),
            'cpc_item_quantity' => $request->input('cpc_item_quantity'),
            'cpc_item_protein' => $request->input('cpc_item_protein'),
            'user_id' => $id,            
            'date' => $request->input('date'),
        ]);
        
        return response()->json(['response'=>'success']);
    }
}

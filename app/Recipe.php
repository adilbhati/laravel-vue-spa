<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    //
    // primary key is defined in custom way instead of id
    protected $primaryKey = 'recipe_id';

    // created_at & updated_at column values defined in custom way
    const CREATED_AT = 'recipe_created';
    const UPDATED_AT = 'recipe_updated';

    protected $fillable = [
        'recipe_name','recipe_url','recipe_energy','recipe_fat','recipe_protein','recipe_carbohydrate','recipe_idealtime'
    ];
    
    //Eloquent Relationships
    //Meal Plan will have multiple recipes
    //Eloquent Format : 'Target Model','This Table','This Id','Target Model Id'
    public function mealplanitem(){
        return $this->belongsTo('App\MealPlanItem','recipes', 'recipe_id','mealplanitem_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{   
    //Define Table name if it shows 1146 error
    //public $table = "questions";

    //custom primary key
    protected $primaryKey = 'question_id';

    //custom timestamps
    const CREATED_AT = 'question_created';
    const UPDATED_AT = 'question_updated';

    protected $fillable = [
        'question_query','question_askedby','questions','question_answeredby','question_answer'
    ];

    //Eloquent Format : 'Target Model','This Table','This Id','Target Model Id'
    public function user(){
        return $this->hasOne('App\User','id','question_askedby');
    }
}

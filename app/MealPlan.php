<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MealPlan extends Model
{
    //created primary key in custom way
    protected $primaryKey = 'mealplan_id';

    //custom timestamps
    const CREATED_AT = 'mealplan_created';
    const UPDATED_AT = 'mealplan_updated';

    protected $fillable = ['mealplan_name','mealplan_description','mealplan_energy'];

    //Eloquent Relationships
    //Meal Plan will have multiple recipes
    //Eloquent Format : 'Target Model','This Table','This Id','Target Model Id'
    public function mealplanitem(){
        return $this->belongsTo('App\MealPlanItem','meal_plans', 'mealplan_id','mealplanitem_id');
    }
}

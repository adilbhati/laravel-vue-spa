<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FoodItem extends Model
{
    //
    // primary key is defined in custom way instead of id
    protected $primaryKey = 'item_id';

    // created_at & updated_at column values defined in custom way
    const CREATED_AT = 'item_created';
    const UPDATED_AT = 'item_updated';
    
    protected $fillable = [
        'item_name','item_colloquial', 'item_pdcaas', 'item_protein','item_deviation','item_units','item_category'
    ];

}

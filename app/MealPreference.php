<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MealPreference extends Model
{
    //created primary key in custom way
    protected $primaryKey = 'mealpreference_id';

    //custom timestamps
    const CREATED_AT = 'mealpreference_created';
    const UPDATED_AT = 'mealpreference_updated';

    protected $fillable = ['mealpreference_name'];

    //Eloquent Relationships
    //Meal Plan will have multiple recipes
    //Eloquent Format : 'Target Model','This Table','This Id','Target Model Id'
    public function userprofile(){
        return $this->belongsTo('App\UserProfile', 'mealpreference_id','profile_id');
    }

}

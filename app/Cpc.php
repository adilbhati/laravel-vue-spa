<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cpc extends Model
{
    //Define Table name if it shows 1146 error
    public $table = "cpc";
    // primary key is defined in custom way instead of id
    protected $primaryKey = 'cpc_id';

    // created_at & updated_at column values defined in custom way
    const CREATED_AT = 'cpc_created';
    const UPDATED_AT = 'cpc_updated';
    //

    protected $fillable = [
        'item_id','cpc_item_quantity','cpc_item_protein','user_id','date'
    ];

    

    //Eloquent Relationships
    //CPC will have multiple food items
    //Eloquent Format : 'Target Model','This Table','This Id','Target Model Id'
    public function fooditem(){
        return $this->belongsToMany('App\FoodItem','cpc', 'cpc_id','item_id');
    }
    
}
